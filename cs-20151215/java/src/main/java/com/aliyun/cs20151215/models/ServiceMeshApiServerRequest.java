// This file is auto-generated, don't edit it. Thanks.
package com.aliyun.cs20151215.models;

import com.aliyun.tea.*;

public class ServiceMeshApiServerRequest extends TeaModel {
    @NameInMap("headers")
    public java.util.Map<String, String> headers;

    public static ServiceMeshApiServerRequest build(java.util.Map<String, ?> map) throws Exception {
        ServiceMeshApiServerRequest self = new ServiceMeshApiServerRequest();
        return TeaModel.build(map, self);
    }

}
