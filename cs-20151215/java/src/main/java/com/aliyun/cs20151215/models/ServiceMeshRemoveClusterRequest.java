// This file is auto-generated, don't edit it. Thanks.
package com.aliyun.cs20151215.models;

import com.aliyun.tea.*;

public class ServiceMeshRemoveClusterRequest extends TeaModel {
    @NameInMap("headers")
    public java.util.Map<String, String> headers;

    public static ServiceMeshRemoveClusterRequest build(java.util.Map<String, ?> map) throws Exception {
        ServiceMeshRemoveClusterRequest self = new ServiceMeshRemoveClusterRequest();
        return TeaModel.build(map, self);
    }

}
