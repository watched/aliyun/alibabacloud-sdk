// This file is auto-generated, don't edit it. Thanks.
package com.aliyun.cs20151215.models;

import com.aliyun.tea.*;

public class ServiceMeshAddClusterRequest extends TeaModel {
    @NameInMap("headers")
    public java.util.Map<String, String> headers;

    public static ServiceMeshAddClusterRequest build(java.util.Map<String, ?> map) throws Exception {
        ServiceMeshAddClusterRequest self = new ServiceMeshAddClusterRequest();
        return TeaModel.build(map, self);
    }

}
