// This file is auto-generated, don't edit it. Thanks.
package com.aliyun.cs20151215.models;

import com.aliyun.tea.*;

public class UpdateServiceMeshRequest extends TeaModel {
    @NameInMap("headers")
    public java.util.Map<String, String> headers;

    public static UpdateServiceMeshRequest build(java.util.Map<String, ?> map) throws Exception {
        UpdateServiceMeshRequest self = new UpdateServiceMeshRequest();
        return TeaModel.build(map, self);
    }

}
