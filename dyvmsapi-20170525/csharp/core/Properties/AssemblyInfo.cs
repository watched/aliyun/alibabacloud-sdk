﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("alibabacloud-sdk-dyvmsapi")]
[assembly: AssemblyDescription("alibabacloud sdk dyvmsapi")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Alibaba Cloud, Inc")]
[assembly: AssemblyProduct("alibabacloud sdk dyvmsapi")]
[assembly: AssemblyCopyright("©2009-present Alibaba Cloud")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("6bdccf30-e435-46db-9551-c33fd05174e9")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("0.0.2.0")]
[assembly: AssemblyFileVersion("0.0.2.0")]
