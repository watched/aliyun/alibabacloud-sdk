module github.com/alibabacloud-go/Facebody-20191230

require (
	github.com/alibabacloud-go/OpenPlatform-20191219 v1.1.0
	github.com/alibabacloud-go/endpoint-util v1.1.0
	github.com/alibabacloud-go/tea v1.1.0
	github.com/alibabacloud-go/tea-fileform v1.1.0
	github.com/alibabacloud-go/tea-oss-sdk v1.1.0
	github.com/alibabacloud-go/tea-oss-utils v1.1.0
	github.com/alibabacloud-go/tea-rpc v1.1.0
	github.com/alibabacloud-go/tea-rpc-utils v1.1.0
	github.com/alibabacloud-go/tea-utils v1.1.0
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
)
