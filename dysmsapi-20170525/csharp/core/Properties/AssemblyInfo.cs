﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("alibabacloud-sdk-dysmsapi")]
[assembly: AssemblyDescription("alibabacloud sdk dysmsapi")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Alibaba Cloud, Inc")]
[assembly: AssemblyProduct("alibabacloud sdk dysmsapi")]
[assembly: AssemblyCopyright("©2009-present Alibaba Cloud")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("3d3ac134-a9eb-49d5-9608-7d9ce480977a")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("0.0.3.0")]
[assembly: AssemblyFileVersion("0.0.3.0")]
